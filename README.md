# [libobs-rs] libobs Bindings for Rust

Use the libobs libary in rust without having to use c.

## Getting Started

These instructions will help you to build the bindings.

### Prerequisites
* [git](https://git-scm.com/)
* [rust](https://www.rust-lang.org/tools/install)                    


### How to use it in your project

1.First git clone this repository in to your development directory:
```
git clone https://gitlab.com/techfreak/libobs-rs
```

2.Generate the docs for libobs-rs:
```
cd libobs-rs
cargo docs
cd ../
```


3.Next create a new libary project with cargo:
```
cargo new <your_plugin_name>  --lib 
```

4.Edit the <your_plugin_name>/Cargo.toml file and add this to dependencies:
```
[dependencies]
libobs = { path = "../libobs-rs/" }
```

4.Now implement the required methods for an obs plugin in your  <your_plugin_name>/src/lib.rs file
```rust
extern crate libobs as plugin;

fn make_string_safe(text:*const c_char) -> String {
    unsafe {
        CStr::from_ptr(text).to_string_lossy().into_owned()
    }
}

fn make_unsafe_string(text:&str) -> *const c_char {
    return CString::new(text).expect("Converting rust string to *char failed.").as_ptr()
}


#[no_mangle]
pub unsafe extern "C" fn obs_module_name() -> *const c_char {
    make_unsafe_string("test")
}

#[no_mangle]
pub unsafe extern "C" fn obs_module_author() -> *const c_char {
    make_unsafe_string("techfreak") 
}

#[no_mangle]
pub unsafe extern "C" fn obs_module_load() -> bool {
    println!("Obs is running version: {:#?}",make_string_safe(plugin::obs_get_version_string()));

    return true;
}

#[no_mangle]
pub unsafe extern "C" fn obs_module_unload(){
    //freeing c pointers
}
#[no_mangle]
pub unsafe extern "C" fn  obs_module_ver ()  {
}

#[no_mangle]
pub unsafe extern "C" fn  obs_module_set_pointer(_module:plugin::obs_module_t)  {
    println!("{:#?}",_module);
}

```

5.Build the plugin:
```sh
cargo build
```

6.Install the plugin by moving the .so file to your plugin folder

7.Profit !


### Alternatives
* [obs-gpmdp/libobs-sys](https://github.com/mdonoughe/obs-gpmdp/tree/master/libobs-sys) is using an old obs version.
* [libobs-sys](https://crates.io/crates/libobs-sys) did not compile for me at the time of writing this guide.


## Authors

* techfreak


## License

This project is licensed under the GPLv2 - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [Southclaws](https://github.com/Southclaws) I started with his version as a base and got it working.
* [mdonoughe](https://github.com/Southclaws) I looked at his project for inspiration how he got the bindings working.
