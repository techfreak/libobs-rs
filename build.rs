extern crate bindgen;
extern crate pkg_config;
use std::env;
use std::path::PathBuf;
use std::process::Command;

use bindgen::callbacks::{MacroParsingBehavior, ParseCallbacks};
#[derive(Debug)]
struct MacroCallback();

impl ParseCallbacks for MacroCallback {
    fn will_parse_macro(&self, name: &str) -> MacroParsingBehavior {
        match name {
            "FP_ZERO" | "FP_SUBNORMAL" | "FP_NORMAL" | "FP_INFINITE" | "FP_NAN" => {
                MacroParsingBehavior::Ignore
            }
            _ => MacroParsingBehavior::Default,
        }
    }
}



fn main() {
    Command::new("git").arg("clone").arg("https://github.com/obsproject/obs-studio").arg("lib/obs-studio").output().expect("failed to clone the obs-studio repo.");

    pkg_config::probe_library("libobs").unwrap();
    //let _ = pkg_config::probe_library("libobs").unwrap();
    // Tell cargo to tell rustc to link the system bzip2
    // shared library. clang
    println!("cargo:rustc-link-lib=dylib=obs");

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .trust_clang_mangling(false)
        .parse_callbacks(Box::new(MacroCallback()))
        .clang_arg("-Ilib/obs-studio/libobs/")
        .header("wrapper.h")
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
